from gasp import *

begin_graphics()

def face(x,y):
    Circle((x, y), 40)
    #200, #200
    Circle((x-15, y+10), 5)
    Circle((x+15, y+10), 5)
    Line((x-10, y-10), (x, y-10))
    Line((x-10, y-10), (x, y+10))
    Arc((x, y), 30, 225, 315)
    Line((x-20,y+20),(x-10,y+20))
    Line((x+10,y+20),(x+20,y+20))
    Arc((x-35,y),20,90,270)
    Arc((x+35,y),20,270,450)

face(100,200)

update_when('key_pressed')  # again, this keeps the graphics window open...
end_graphics()

