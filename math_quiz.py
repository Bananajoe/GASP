from random import randint

right = 0

for x in range(0,10):
    num1 = randint(1, 10)
    num2 = randint(1, 10)
    answer = num1 * num2
    question = "What is " + str(num1) + " times " + str(num2) + "? "
    responce = int(input(question))
    if answer == responce:
        print ("That's right - well done.")
        right = right + 1
    else:
        print ("No I'm afraid the answer is " + str(answer) + ".")

print("I asked you 10 questions.  You got " + str(right) + " of them right.")
print("Well done!")
